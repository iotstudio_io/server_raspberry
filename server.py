from flask import Flask, request, Response
import json
import time
import serial
import os
import base64

PRODUCTION = False


app = Flask(__name__)

p_test = '/dev/ttyACM0'
p_production = '/dev/ttyUSB0'


if PRODUCTION:
    is_port_production = os.system('ls ' + p_production)
    os.system("sudo chmod 666 " + p_production)
    port = p_production
else:
    is_port_test = os.system('ls ' + p_test)
    os.system("sudo chmod 666 " + p_test)
    port = p_test

dev_weighing_machine = serial.Serial(
        baudrate=9600,
        bytesize=8,
        stopbits=1,
        timeout=0.1,
        port=port
    )

def capture_info():

    lec = dev_weighing_machine.readline()

    b = ''
    for j in range(len(lec)):
        if lec[j] not in '0123456789':
            b = j
            break
    print b, lec

    dev_weighing_machine.flushInput()

    # dev_weighing_machine.close()
    PESO = lec[:b]
    UNIDAD_PESO = lec[b:].split('\n')
    UNIDAD_PESO = UNIDAD_PESO[0].strip()

    # os.system("fswebcam -r 1280x720 --no-banner /home/pi/scope1.jpg -d /dev/video0")
    # os.system("fswebcam -r 1280x720 --no-banner scope2.jpg -d /dev/video1")

    if PRODUCTION:
        os.system("streamer -c /dev/video0 -t 5 -r 2 -o /home/pi/scopex0.jpeg")
        time.sleep(2)
        os.system("streamer -c /dev/video1 -t 5 -r 2 -o /home/pi/scopey0.jpeg")
        time.sleep(2)
    else:
        os.system("streamer -c /dev/video0 -t 5 -r 2 -o /home/pi/scopex0.jpeg")
        time.sleep(2)
        os.system("streamer -c /dev/video1 -t 5 -r 2 -o /home/pi/scopey0.jpeg")
        time.sleep(2)

    ID = '2'
    # image_file1 = ""
    # image_file2 = ""
    image_file1 = open('/home/pi/scopex4.jpeg', 'rb')
    img_data1 = image_file1.read()

    image_file2 = open('/home/pi/scopey4.jpeg', 'rb')
    img_data2 = image_file2.read()

    DATE_TODAY = time.strftime("%y/%m/%d %H:%M:%S")

    data = {
        # 'Date': DATE_TODAY,
        'Image1': base64.b64encode(img_data1),
        'Image2': base64.b64encode(img_data2),
        'FormatoImagen': 'JPG',
        'Id': ID,
        'Peso': PESO,
        'UnidadPeso': UNIDAD_PESO
    }
    js = json.dumps(data)

    image_file1.close()
    image_file2.close()

    resp = Response(js, status=200, mimetype='application/json')

    return resp

@app.route('/capture', methods=['GET'])
def captureIn():
    if request.method == 'GET':
        resp = capture_info()
        os.system("sudo rm -r /home/pi/*.jpeg")
        print "Image delete"
        return resp

if __name__ == '__main__':
    app.run(host='0.0.0.0',port=5000,debug=True)
