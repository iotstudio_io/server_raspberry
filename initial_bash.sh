#! /bin/sh
# /etc/init.d/initial_bash

### BEGIN INIT INFO
# Provides:          initial_bash
# Required-Start:    $all
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Script de ejemplo de arranque automático
# Description:       Script para arrancar el detector de presencia
### END INIT INFO


# Dependiendo de los parámetros que se le pasen al programa se usa una opción u otra
case "$1" in
  start)
    echo "Arrancando initial_bash"
    # Aquí hay que poner el programa que quieras arrancar automáticamente
    cd /home/pi/  && /usr/bin/python  server.py
    ;;
  stop)
    echo "Deteniendo initial_bash"

    ;;
  *)
    echo "Modo de uso: /etc/init.d/initial_bash {start|stop}"
    exit 1
    ;;
esac

exit 0